import os
import time
import requests
import discord
import asyncio

DISCORD_TOKEN=os.environ["DISCORD_TOKEN"]
XNOAI_APIKEY=os.environ["XNOAI_APIKEY"]

class XNOAIClient(discord.Client):
    async def on_ready(self):
        print(f'Logged in as {self.user} (ID: {self.user.id})')
        print('------')

    async def on_message(self, message):
        if message.content.startswith('x '):
            prompt_text = message.content[2:]
            headers = {"X-API-KEY": XNOAI_APIKEY}
            prompt = requests.post("https://xno.ai/api/prompts", data={"limit": 1, "text": prompt_text}, headers=headers).json()
            await message.channel.send("working on '"+prompt_text+"'")
            while 'queued' in prompt['image_status'] or 'generating' in prompt['image_status']:
                print("Waiting for render")
                time.sleep(5)
                prompt = requests.get("https://xno.ai/api/prompts/"+prompt["uuid"], headers=headers).json()
            for img in prompt['images']:
                embed = discord.Embed(title=prompt_text, description="", color=0x00ff00)
                embed.set_image(url=img)
                await message.channel.send(embed=embed)

intents = discord.Intents.default()
intents.message_content = True

client = XNOAIClient(intents=intents)
client.run(DISCORD_TOKEN)
