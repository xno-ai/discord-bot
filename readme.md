Setup instructions:

* Set up your discord bot and get the token from [https://realpython.com/how-to-make-a-discord-bot-python/](https://realpython.com/how-to-make-a-discord-bot-python/)
* Get your XNOAI_APIKEY from [xno.ai](https://xno.ai)
* run: DISCORD_TOKEN=... XNOAI_APIKEY=... python3 discord_bot.py
* in discord: try 'x cat bird' as a prompt
